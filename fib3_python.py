'''@author James Makau
  Calculate the 4000'th fib3 number.
  Fib3 is defined as a Fibonacci sequence that uses the last 3 numbers 
  to generate the next one (as opposed to the regular Fib)
  So the first 6 numbers from this sequence will be 1 1 1 3 5 9
'''
print   "In the fib3 sequence: 1, 1, 1, 3, 5, 9 ... "
print "The 4000th number is : ";

a = 1
b = 1
c = 1
d = 3
e = 5
f = 9

x = 0
nth = 7

while True:
    x = d + e + f
    if nth == 3994:
        print x
        exit
    d = e
    e = f
    f = x
    nth +=1
