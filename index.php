<?php

/*
 * @author James Makau
 * Calculate the 4000'th fib3 number.
 * Fib3 is defined as a Fibonacci sequence that uses the last 3 numbers 
 * to generate the next one (as opposed to the regular Fib)
 * So the first 6 numbers from this sequence will be 1 1 1 3 5 9 
 * 
 */

print  "In the fib3 sequence: <strong>1, 1, 1, 3, 5, 9 ... <br /></strong> <br />
The 4000th number is : <br /><br />";
$a = 1;  // 1st
$b = 1;  // 2nd
$c = 1;  // 3rd
$d = 3;  // 4th
$e = 5;  // 5th
$f = 9;  // 6th

$x = 0;
$nth = 7;
while(true) {

    $x = bcadd(bcadd($d , $e) , $f);
    //echo($x."<br />");
    if($nth == 3994)  // 4000 - 6
    {
    echo($x."<br />");
    exit();
    }
    $d = $e;
    $e = $f;
    $f = $x;
    $nth++;
}

?>